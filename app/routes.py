from flask_restful import Api
from .resources import PersonListResource, PersonResource

def add_routes(app):
  api = Api(app)

  api.add_resource(PersonListResource, '/person')
  api.add_resource(PersonResource, '/person/<person_id>')

  return api

