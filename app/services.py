from .models import Person

class PersonService:
  @staticmethod
  def get(person_id):
    return Person.get(person_id)

  @staticmethod
  def put(person_id, payload):
    return Person.update(person_id, payload)

  @staticmethod
  def patch(person_id, payload):
    return Person.update(person_id, payload)  

  @staticmethod
  def delete(person_id):
    return Person.delete(person_id)

  @staticmethod
  def all():
    return Person.all()
  
  @staticmethod
  def post(payload):
    return Person.save(payload)
