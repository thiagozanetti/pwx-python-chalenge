from ast import literal_eval
from .database import DB


class PersonModel(DB.Model):
    id = DB.Column(DB.Integer, primary_key=True)
    name = DB.Column(DB.String(150), unique=True, nullable=False)
    ssn = DB.Column(DB.String(11), unique=True, nullable=False)
    email = DB.Column(DB.String(150), unique=False, nullable=False)
    photo = DB.Column(DB.String(256), unique=False, nullable=True)
    birthday = DB.Column(DB.DateTime, unique=False, nullable=False)

    def save(self):
        DB.session.add(self)
        DB.session.commit()

        return self

    def delete(self):
        DB.session.delete(self)
        DB.session.commit()

        return self


class Person(object):
    @staticmethod
    def format(person):
        return {
            "id": person.id,
            "name": person.name,
            "ssn": person.ssn,
            "email": person.email,
            "photo": person.photo,
            "birthday": person.birthday.strftime("%Y-%m-%d")
        }

    @staticmethod
    def get(person_id):
        person = PersonModel.query.filter_by(id=person_id).first()

        return Person.format(person)

    @staticmethod
    def all():
        result = []

        for person in PersonModel.query.all():
            result.append(Person.format(person))

        return result

    @staticmethod
    def save(payload_str):
        payload = literal_eval(payload_str)

        person = PersonModel(**payload).save()

        return Person.format(person)

    @staticmethod
    def update(person_id, payload_str):
        payload = literal_eval(payload_str)

        person = PersonModel.query.filter_by(id=person_id).first()

        person.name = payload["name"]
        person.ssn = payload["ssn"]
        person.email = payload["email"]
        person.photo = payload["photo"]
        person.birthday = payload["birthday"]

        person.save()

        return Person.format(person)

    @staticmethod
    def delete(person_id):
        person = PersonModel.query.filter_by(id=person_id).first()

        person.delete()

        return {}
