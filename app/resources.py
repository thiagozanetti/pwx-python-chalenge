from flask_restful import Resource, abort, reqparse
from .services import PersonService

parser = reqparse.RequestParser()
parser.add_argument("person")

class PersonResource(Resource):
  def abort_if_not_found(self, person_id):
    if PersonService.get(person_id) is None:
      abort(404, message="Person {} doesn't exist".format(person_id))

  def get(self, person_id):
    self.abort_if_not_found(person_id)
    
    return PersonService.get(person_id)

  def put(self, person_id):
    self.abort_if_not_found(person_id)

    payload = parser.parse_args()

    return PersonService.put(person_id, payload['person']), 201

  def patch(self, person_id):
    self.abort_if_not_found(person_id)

    payload = parser.parse_args()

    return PersonService.patch(person_id, payload['person']), 201

  def delete(self, person_id):
    self.abort_if_not_found(person_id)

    return PersonService.delete(person_id), 204


class PersonListResource(Resource):
  def post(self):
    payload = parser.parse_args()

    return PersonService.post(payload['person']), 201

  def get(self):
    return PersonService.all()

