import os

def env(name):
    try:
        return os.environ[name]
    except KeyError:
        message = "Expected environment variable '{}' is not set.".format(name)
        raise Exception(message)

DB_NAME = env("DB_NAME")
DB_USER = env("DB_USER")
DB_PASS = env("DB_PASS")
DB_ADDR = env("DB_ADDR")

DB_URL = 'postgresql+psycopg2://{user}:{pw}@{url}/{db}'.format(user=DB_USER, pw=DB_PASS, url=DB_ADDR, db=DB_NAME)

DEBUG = True
HOST = '0.0.0.0'