from flask_sqlalchemy import SQLAlchemy
from .settings import DB_URL

DB = SQLAlchemy()


def db_connection(app):
    app.config['SQLALCHEMY_DATABASE_URI'] = DB_URL
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

    DB.init_app(app)

    initialize_schema(app, DB)

    return DB

def initialize_schema(app, db):
  db.create_all(app=app)


