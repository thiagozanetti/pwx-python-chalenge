FROM python:3.6

LABEL Author="Thiago Zanetti <thiago.project@gmail.com>"
LABEL Name="pwx-python-chalenge" Version=0.1.0

EXPOSE 5000

WORKDIR /pwx
ADD . /pwx

ENV DB_NAME=pwx
ENV DB_USER=postgres
ENV DB_PASS=postgres
ENV DB_ADDR=db

RUN apt-get update 
RUN apt-get install libpq-dev python3-dev build-essential -y

RUN python3 -m pip install --upgrade pip
RUN python3 -m pip install -r requirements.txt

CMD ["python3", "app.py"]
