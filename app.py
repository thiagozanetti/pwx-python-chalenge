from flask import Flask
from app import routes, settings, database

app = Flask(__name__)

database.db_connection(app)
routes.add_routes(app)

@app.route('/')
def root():
  return 'Hello World!'

if __name__ == '__main__':
    app.run(host=settings.HOST, debug=settings.DEBUG)

